package com.dummy.restapiexample.restapiexample.controller;

import com.dummy.restapiexample.restapiexample.model.CommonResponse;
import com.dummy.restapiexample.restapiexample.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/api/employees")
    public CommonResponse getResponse() {
        return employeeService.getResponse();
    }

}
