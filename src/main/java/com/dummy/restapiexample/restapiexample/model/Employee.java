package com.dummy.restapiexample.restapiexample.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * {
 *       "id": "1",
 *       "employee_name": "Tiger Nixon",
 *       "employee_salary": "320800",
 *       "employee_age": "61",
 *       "profile_image": ""
 *     },
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    private String id;
    private String employeeName;
    private String employeeAge;
    private String profileImage;


}
