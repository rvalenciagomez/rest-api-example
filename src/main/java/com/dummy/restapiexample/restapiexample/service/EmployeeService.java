package com.dummy.restapiexample.restapiexample.service;

import com.dummy.restapiexample.restapiexample.model.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EmployeeService {

    @Autowired
    private RestTemplate restClient;

    public CommonResponse getResponse() {
        //http://dummy.restapiexample.com/api/v1/employees
        return restClient.getForObject("http://dummy.restapiexample.com/api/v1/employees", CommonResponse.class);
    }

}
