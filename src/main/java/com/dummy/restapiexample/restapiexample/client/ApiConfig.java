package com.dummy.restapiexample.restapiexample.client;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApiConfig {

    @Bean
    public RestTemplate registerRestTemplate() {
        return new RestTemplate();
    }
}
